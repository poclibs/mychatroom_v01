package com.potingchiang.android.practice.mychat.model;

import com.potingchiang.android.practice.mychat.visitorbinder.Visitable;
import com.potingchiang.android.practice.mychat.visitorbinder.VisitorBinder;

/**
 * Mgs is an object class for message transmit between client-server information
 * Created by potingchiang on 2018-02-25.
 */

public class Msg implements Visitable {

    // fields
    private String message;
    private User sender;
    private long createdAt;

    // constructor
    public Msg() {
    }

    public Msg(String message, User sender, long createdAt) {
        this.message = message;
        this.sender = sender;
        this.createdAt = createdAt;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public User getSender() {
        return sender;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }

    public long getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(long createdAt) {
        this.createdAt = createdAt;
    }

    @Override
    public void accept(VisitorBinder visitorBinder) {

        visitorBinder.visit(this);
    }
}
