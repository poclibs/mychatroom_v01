package com.potingchiang.android.practice.mychat.visitorbinder;

/**
 * interface for visitor design pattern - element
 * Created by potingchiang on 2018-02-26.
 */

public interface Visitable {

    void accept(VisitorBinder visitorBinder);
}
