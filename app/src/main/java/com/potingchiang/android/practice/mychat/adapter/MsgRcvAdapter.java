package com.potingchiang.android.practice.mychat.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.potingchiang.android.practice.mychat.visitorbinder.VisitorBinder;
import com.potingchiang.android.practice.mychat.model.Msg;

import java.util.List;

/**
 * recycler view adapter for message sending and receiving
 * Created by potingchiang on 2018-02-25.
 */

public class MsgRcvAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    // constants for view type
    public static final int VIEW_TYPE_MSG_SENT      = 1;
    public static final int VIEW_TYPE_MSG_RECEIVED  = 2;
    // fields
    // msg list
    private List<Msg> mMsgList;
    // data binder
    private static VisitorBinder sMVisitorBinder;

    // constructor
    public MsgRcvAdapter(List<Msg> msgList, VisitorBinder VisitorBinder) {
        mMsgList = msgList;
        sMVisitorBinder = VisitorBinder;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 0;
    }

    @Override
    public int getItemViewType(int position) {
        return super.getItemViewType(position);
    }

    // sent view holder
    static class MsgSentViewHolder extends RecyclerView.ViewHolder {

        public MsgSentViewHolder(View itemView) {
            super(itemView);
        }
    }
    // received view holder
    static class MsgReceivedViewHolder extends RecyclerView.ViewHolder {

        public MsgReceivedViewHolder(View itemView) {
            super(itemView);
        }
    }
}
