package com.potingchiang.android.practice.mychat.visitorbinder;

import android.view.View;

import com.potingchiang.android.practice.mychat.model.Msg;

/**
 * interface for visitor design pattern & bind onClick listener for recycler adapter - visitor
 * Created by potingchiang on 2018-02-25.
 */

public interface VisitorBinder extends View.OnClickListener {

    void visit(Msg msg);
    void bind(final View.OnClickListener clickListener);
}
