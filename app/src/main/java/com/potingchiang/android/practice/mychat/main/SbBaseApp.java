package com.potingchiang.android.practice.mychat.main;

import android.app.Application;

import com.sendbird.android.SendBird;

/**
 * This is base application to run/initiate before any of the application components
 * when the app is started
 * which is declared in <application android:name></application> attribute in manifest file
 * Created by potingchiang on 2018-02-27.
 */

public class SbBaseApp extends Application {

    // constant
    private static final String APP_ID = "6DA11D02-AEC5-48ED-A64D-C4943BCC9EE9";
    public static final String VERSION = "1.0.0";

    // call onCreate to initiate
    @Override
    public void onCreate() {
        super.onCreate();

        SendBird.init(APP_ID, getApplicationContext());
    }

}
