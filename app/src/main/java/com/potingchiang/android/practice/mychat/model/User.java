package com.potingchiang.android.practice.mychat.model;

/**
 * User is a object class for a person using the service
 * Created by potingchiang on 2018-02-25.
 */

public class User {

    // fields
    private String name;
    private String url;

    // constructor
    public User() {
    }

    public User(String name, String url) {
        this.name = name;
        this.url = url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
