# MyChatRoom_v01

This is demo. chat room app using SendBird backend api

# Copyright
1. Orignal codes belong to Poting Chiang
2. The SendBird demo. source codes and api/endpoint codes credits and copyright by SendBird

# Reference
* SendBird: https://docs.sendbird.com/android
* SendBird blog: https://blog.sendbird.com/android-chat-tutorial-building-a-messaging-ui
* Antonio Leiva: https://antonioleiva.com/recyclerview-listener/
